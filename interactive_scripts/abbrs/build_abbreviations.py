#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Converts an abbreviations listing text file to Vim plugin syntax

* Looks for "abbreviations" file in the same directory as this script
* Expects file in UTF-8 format
"""

from os import path
import shutil


class FTPluginAbbreviationEditor:
    """Perform easy updates to the abbreviations list to a ftplugin

    Args:
        plugin (str): name of the plugin file, e.g., markdown.vim

    Attributes:
        plugin (str): name of the plugin file, e.g., markdown.vim
        plugin_path (str): full path of the plugin file
        ft_dir (str): full path to the ftplugin directory
        abbrevs_path (str): full path to abbreviations file
        add_abbrs (list): lines of adding abbrevs in vimscript
        undo_abbrs (list): lines of undoing abbrevs in vimscript
    """

    def __init__(self, plugin):
        # get the ftplugin directory...the tree will look like the following:
        #
        # dict_resources/
        # ├── abbreviations
        # ├── build_abbreviations.py
        # ├── build_dicts.py
        # ftplugin/
        # ├── filetype1.vim
        # └── filetype2.vim
        #
        # so we first get the absolute path of the directory dict_resources,
        # then we move up one dir with path.split and go down to ftplugin/
        self.script_dir = path.dirname(path.abspath(__file__))
        self.ft_dir = path.join(path.split(self.script_dir)[0], "ftplugin")

        self.plugin = plugin
        self.plugin_path = path.join(self.ft_dir, self.plugin)
        self.abbrevs_path = path.join(self.script_dir, "abbreviations")
        self.add_abbrs = []
        self.undo_abbrs = []

    def make_vimscript_code(self):
        """Makes new vimscript code from abbreviations file"""

        with open(self.abbrevs_path, 'r') as abbr_file:
            abbreviations = [a.strip().split(maxsplit=1)
                             for a in abbr_file.readlines()]

        # outputs for vim plugin
        self.add_abbrs = []
        self.undo_abbrs = []
        for abbr in abbreviations:
            add_abbr = "iabbr <buffer> {} {}".format(abbr[0], abbr[1])
            undo_abbr = "{}\\ \" iunabbr <buffer> {} \" .".format(
                "    " * 5, abbr[0])
            self.add_abbrs.append(add_abbr)
            self.undo_abbrs.append(undo_abbr)

    def visualize_edits(self):
        """Prints new abbreviation plugin code to the console"""

        print("Add abbreviations:")
        for abbr in self.add_abbrs:
            print(abbr)

        print("Undo ftplugin:")
        for undo_abbr in self.undo_abbrs:
            print(undo_abbr)

    def edit_ftplugin(self):
        """Adds vimscript code to appropriate plugin

        This function will do all of the necessary parsing to remove the
        abbreviation addition/removal lines and insert the new ones. These
        contents will be written out to a temporary file, then it will
        backup the original vim plugin file. Finally, the temporary file
        will be renamed in order to overwrite the original vim plugin,
        then the temporary file will be removed itself.
        """

        # read in the plugin file and automatically remove newlines
        with open(self.plugin_path, 'r') as plugin_file:
            contents = [a.strip('\n') for a in plugin_file.readlines()]

        #######################
        ### PARSE/EDIT FILE ###
        #######################

        # these parts must be in this order
        indices = [0, 0, 0, 0]

        # get indices of all important lines
        for i, line in enumerate(contents):
            if line == '" >>>ABBREVIATE START<<<':
                if sum(indices[0:]):
                    print("Error: ABBREVIATE START in wrong order!")
                indices[0] = i
            elif line == '" >>>ABBREVIATE END<<<':
                if sum(indices[1:]):
                    print("Error: ABBREVIATE END in wrong order!")
                indices[1] = i
            elif line == '" >>>UNABBREVIATE START<<<':
                if sum(indices[2:]):
                    print("Error: UNABBREVIATE START in wrong order!")
                indices[2] = i
            elif line == '" >>>UNABBREVIATE END<<<':
                if sum(indices[3:]):
                    print("Error: UNABBREVIATE END in wrong order!")
                indices[3] = i

        if any([a == 0 for a in indices]):
            print("One or more indices not found!")

        # add abbreviations in the appropriate parts
        contents = contents[:indices[0] + 1] + self.add_abbrs + \
                   contents[indices[1]:indices[2] + 1] + self.undo_abbrs + \
                   contents[indices[3]:]

        #############################
        ### FILE I/O with backups ###
        #############################

        # write to a different file for safety
        # https://stackoverflow.com/questions/41915140/delete-the-contents-of-a-file-before-writing-to-it-in-python#comment71011705_41915140
        tmp_file_path = path.join(self.ft_dir, self.plugin + "tmpzzz")
        with open(tmp_file_path, 'w') as tmp:
            print("\n".join(contents), file=tmp)

        # backup original file as well with `*.backup`
        shutil.copy(self.plugin_path,
                    path.join(self.ft_dir, self.plugin + ".backup"))

        # rename backup file to overwrite original
        shutil.move(tmp_file_path, self.plugin_path)


def main():  # pylint: disable=missing-docstring
    abbr_editor = FTPluginAbbreviationEditor("markdown.vim")
    abbr_editor.make_vimscript_code()
    abbr_editor.visualize_edits()
    abbr_editor.edit_ftplugin()


if __name__ == "__main__":
    main()
