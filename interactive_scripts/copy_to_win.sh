#!/bin/bash

zip -r ../vimfiles .           \
    --exclude .git/\*          \
    --exclude dicts/\*         \
    --exclude *original/\*     \
    --exclude copy_to_win.sh

echo "Copying to windows"
cp ../vimfiles.zip /mnt/c/Users/JakeD/

#echo "backing up old vimfiles"
#cp -r /mnt/c/Users/JakeD/vimfiles /mnt/c/Users/JakeD/vimfilesold

echo "unzipping into vimfiles folder"
mkdir /mnt/c/Users/JakeD/vimfiles
unzip -uo /mnt/c/Users/JakeD/vimfiles.zip -d /mnt/c/Users/JakeD/vimfiles/

# TODO: Run build_dicts.py script to generate dictionaries
