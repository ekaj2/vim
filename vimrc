" setup pathogen stuff by tpope
execute pathogen#infect()

" for YouCompleteMe plugin
let g:ycm_server_python_interpreter="/usr/bin/python"

" disable for now
" let g:loaded_youcompleteme = 1

" use pylint for syntastic
" let g:syntastic_python_checkers = ["pylint"]

" make vim...vim
set nocompatible

" enable filetype plugin
filetype on
filetype plugin on

" custom filetypes
au BufRead,BufNewFile *.qnote setfiletype qnote

" setup the bash environment (so we can use aliases)
" https://stackoverflow.com/a/18901595
" https://stackoverflow.com/a/19819036
let $BASH_ENV="~/.bash_aliases"

" enable project specific vimrc's
" https://andrew.stwrt.ca/posts/project-specific-vimrc/
"set exrc
set secure
" http://vim.wikia.com/wiki/Project_specific_settings
silent! so .vimlocal

" ====================== "
"     STYLE SETTINGS     "
" ====================== "

" persistent undo
set undofile
" set undodir=~/.vim/undofiles,~\\vimfiles\\undofiles,.

" use unix file format
set fileformat=unix

" code folding (default to open all folds)
set foldmethod=marker
set foldlevelstart=99
set foldopen-=block

" don't wrap, use zl, zh, zL, zH to navigate
" sidescroll controls when the cursor goes off of the screen
" zl and zh can have a number prepended, e.g., 5zl
set nowrap
set sidescroll=5

" autowrap git commit message edits
autocmd FileType gitcommit setlocal spell

" show command while typing (e.g., 'dw')
set showcmd

" automatically read a file if it had now changes in Vim
" see: https://stackoverflow.com/a/20418591/9974752
set autoread
"autocmd FocusGained,BufEnter,CursorHold,CursorMoved * :checktime

" show line number, column number and % in file
set ruler

" turn on highlighting and search as we type
" turn off highlighting with :noh
set hlsearch
set incsearch

" python indentation
set tabstop=4
set shiftwidth=4
set autoindent
set expandtab

" color theme, use 256-color terminal
syntax on
colo deus
set t_Co=256

" WARNING: this must come after ':syntax on'!
"
" WARNING and NYI are examples, replace/add as needed
" references:
" :h syntax
" :h syn-containedin
"     * /included inside an already existing item
"     * this doesn't seem to actually work...but looks a bit simpler for this
"       use case
" http://learnvimscriptthehardway.stevelosh.com/chapters/45.html
" http://learnvimscriptthehardway.stevelosh.com/chapters/46.html
" https://vi.stackexchange.com/a/15531 <-- much better than vvv
" https://stackoverflow.com/questions/4097259/in-vim-how-do-i-highlight-todo-and-fixme#26942850
"
" share syntax visualization with: ':runtime! syntax/2html.vim'
" see .vim/demos/syntax-keyword-demo.html
augroup MySpecialSyntax
    autocmd!

    " simple, built-in style matching behaviour
    autocmd Syntax * syntax match mySpecialTodos /\v<(WARNING|NYI)>/ containedin=.*Comment,vimCommentTitle contained

    " match to the end of the line if the word is immediately follwed by a
    " single colon and at least one space (': ')
    "
    " because keyword has priority over match, we need to match the character
    " before the regex-keyword, then use an offset (hs=s+1)
    "
    " this one could have :{0,1} to allow for an optional, instead of required
    " colon following the keyword if desired
    autocmd Syntax * syntax match mySpecialTodos /\v.<(WARNING|NYI|FIXME|NOTES|NOTE|XXX|TODO): .*/hs=s+1 containedin=.*Comment,vimCommentTitle contained
    
    " multi-line note in comment block
    " allow whitespace, but nothing else, after BEGINNOTE or ENDNOTE flag
    autocmd Syntax * syntax region mySpecialTodos start=/\v.*<BEGINNOTE\s*$/ end=/\v<ENDNOTE\s*$/ containedin=.*Comment,vimCommentTitle contained
augroup END
highlight def link mySpecialTodos Todo

" I was originally using this to set the GUI font, but I realized that I
" wouldn't have a consistent font across the operating systems that I use, so
" it is better to put these in the ~/.gvimrc file so that it is out of the Git
" repository.
"
" " set font for GVim
" set guifont=Source_Code_Pro_Semibold:h14:cANSI:qDRAFT

" " set font for MacVim
" set guifont=Menlo\ Regular:h16

" enable backspace
set backspace=indent,eol,start

" set autocomplete menu colors (unneeded with good theme and 256-co term)
"highlight Pmenu ctermfg=red ctermbg=darkgray
"highlight PmenuSel ctermfg=blue ctermbg=grey cterm=standout
"highlight PmenuThumb ctermbg=grey
"highlight PmenuSbar ctermbg=black

" ===================== "
"     CURRENT OTHER     "
" ===================== "

" TODO - try to force block cursor shape (not working ATM)
" http://vim.wikia.com/wiki/Configuring_the_cursor
"if &term =~ "xterm\\|rxvt"
"    let &t_EI .= "\<Esc>[2 q"
"endif

" show cursorline on current window
" https://stackoverflow.com/a/12018552
augroup CursorLine
    au!
    au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
    au WinLeave * setlocal nocursorline
augroup END

" enable built-in filetype indentation
filetype indent on

" add english dictionary for autocompletion
"set dictionary+=~/.vim/dicts/english
set dictionary+=~/.vim/dicts/common-english

" autocomplete to the longest ambiguous section of the word
" http://vim.wikia.com/wiki/Make_Vim_completion_popup_menu_work_just_like_in_an_IDE
set completeopt=longest,menuone,preview
" use this if completion is slow with tags and includes, see :h 'complete'
" set complete-=t,i

" confirmation for if :q or similar fails
set confirm

" increase history just in case
set history=250

" make vertical splits open at the right
set splitright

" search down into subfolders
" provides tab-completion for all file-related tasks
" use the ':find something' command to see what it does
" taken from 'How to Do 90% of What Plugins Do (With Just Vim)'
set path+=**

" better autocompletion...
" see SO answer: TODO
set wildmode=longest,list,full
set wildmenu

" disable annoying bells
set visualbell
set vb t_vb=

" case insensitive search --> use \C to make case sensitive
" for a search, you could add an I at the end for sensitivity or use \C
" :%s/lowercasesearch/replaceStr/gI or :%s/lowercasesearch\C/replaceStr/g
set ignorecase
set smartcase

" ======================= "
"     CUSTOM MAPPINGS     "
" ======================= "
" 
" Insert Mode
" -----------
" F4 : add curly braces block
" 
" Visual Mode
" -----------
" F4 : surround selection with curly braces
" 
" Normal Mode
" -----------
" ; : write current buffer
" Alt+] : open ctag reference in new tab

" C# braces hotkey
" TODO - replace with better key
"
" for the vmap:
" g<S-p> puts the text before the cursor, see :h gP
imap <F4> <ESC>o{<CR><tab><CR>}<ESC><<kA
" currently fails if selected text includes last line of file
" because the 'k' goes up and puts the line above the highest 
" selected one below all of the selection and braces
vmap <F4> dki<F4><ESC>PV?{%k>?{%k"_dd%
"      - needs cursor somewhere on previous line (issue?)
"      - fails with the following case (funct indents too far)
"        if (bool || 
"            otherbool )
"            funct();
vmap <F5> dki<F4><ESC>g<S-p><S-A>

" use arrow keys to scroll through tabs
nnoremap <left> :tabp<cr>
nnoremap <right> :tabn<cr>

" TODO - make shift+space exit insert mode (not working in the terminal)
" map escape to something easy
inoremap jk <esc>
" inoremap kj <esc>
inoremap JK <esc>
inoremap KJ <esc>
inoremap Jk <esc>
inoremap Kj <esc>
" from: http://vim.wikia.com/wiki/Avoid_the_escape_key
" make 'Enter' exit insert mode (use Ctrl-J, or Ctrl-Enter if
" needing to actually add a newline in insert mode)
" this one doesn't work right now because of the MarkdownNewline trap for <cr>
" imap <cr> <esc>
" map shift spacebar to escape insert mode
"inoremap <S-space> <esc>

"" newline at end of file
"nnoremap <Leader>l Go

" make ; save the file
" nnoremap ; :w<CR>
"
" swap ; and :
"
" after reading: https://stackoverflow.com/a/19520052
" we need to add the mapping to solve the :ls issue
" nnoremap gb :ls<cr>:b 
" nnoremap ; :
" nnoremap : ;

" TODO - save the file and fix all paths
"function! SaveAndFixPaths(pth)
"    !mkdir -p a:pth
"    w a:pth
"endfunction

" todo/done hotkeys
"let @d = 'RDONE^'
"vmap <S-t> :normal @d<CR>
vmap <S-t> :s/TODO/DONE/g<CR>
imap <Leader>td <ESC>oTODO - 

" ctags --> open in new tab
nnoremap <A-]> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>

" auto-run macro in f because qf is really easy to type
" @q is easy to type, so I'm leaving that separate
nnoremap <space> @f

" vimgrep helper
"au Filetype cs nnoremap <buffer> <Leader>g :vimgrep /<cword>/ **/*.cs
autocmd Filetype cs nnoremap <buffer> <Leader>grep :vimgrep /\<\>/ **/*.cs<CR>:cw<CR>
autocmd Filetype cs nnoremap <buffer> <Leader>gmore :vimgrep // **/*.cs<CR>:cw<CR>

" fix line endings
nnoremap <Leader>fix :%s/\r//g<Bar>w<CR>

" vim web browser
nnoremap <Leader>ddg :!lynx -accept_all_cookies https://duckduckgo.com/lite<CR><CR>

" set width to 80 characters
nnoremap <Leader>80 :vertical res 80<CR>

" open plaintext version of Reddit's helpwithhelp page in vertical split
nmap <Leader>help :vsp ~/.vim/snippets/helpwithhelp.md<cr>

" mac copy
" TODO - only register this mapping on OSX
nnoremap <Leader>cpbuffer :w !pbcopy<CR><CR>

" ====================
"    Git Shortcuts
" ====================
"
" TODO - make these work if we are not cd into git repository

fun! GoToFolderAndRunThenReturn(mycommand)
    " mycommand will get run like:
    " :!<mycommand>
    let gitdir = expand("%:h")

    " from: https://stackoverflow.com/a/4597149
    " don't do :cd `path`
    exe 'cd' fnameescape(gitdir)

    " run command
    exe ":!" . a:mycommand

    " return to previous directory
    silent! cd -
endfun

" git add -p <filename>
" need to expand to full path in case we are in a parent folder
command! Gitafile call GoToFolderAndRunThenReturn("clear;git add -p ". expand("%:p"))

" git add -p (all)
command! Gitaall call GoToFolderAndRunThenReturn("clear;git add -p")

" git add . 
command! Gitaeverything call GoToFolderAndRunThenReturn("git add .")

" open git diff in less, then return immediately to vim
" we need these mappings so we can disable the 'Press ENTER...' prompt
nnoremap <script> <plug>gitdiff :call GoToFolderAndRunThenReturn("git diff --color=always<Bar>less -r")<cr><cr>
command! Gitdiff exe "normal <plug>gitdiff"
nnoremap <script> <plug>gitlog :call GoToFolderAndRunThenReturn("clear;git log --decorate --oneline --all --graph --color=always <bar> less -r")<cr><cr>
command! Gitlog exe "normal <plug>gitlog"

" other simple git commands
command! Gitcommit call GoToFolderAndRunThenReturn("git commit -v")
command! Gitstatus call GoToFolderAndRunThenReturn("clear;git status")
command! Gitbranch call GoToFolderAndRunThenReturn("clear;git branch -av")
command! Gitreset call GoToFolderAndRunThenReturn("clear;git reset")
command! Gitpush call GoToFolderAndRunThenReturn("clear;git push origin HEAD")

fun! AnyCommandInFolder()
    call inputsave()
    let mycomm = input('enter command: ')
    call inputrestore()

    " run the command in the folder of the current buffer
    call GoToFolderAndRunThenReturn("clear;" . mycomm)
endfun
" run any command in the folder of the current buffer
" I don't have the 'u' in the mapping because it is kinda hard to get to
nnoremap <Leader>rn :call AnyCommandInFolder()<cr>


" vanilla linter
" TODO - put this in a python.vim ftplugin
" https://gist.github.com/romainl/ce55ce6fdc1659c5fbc0f4224fd6ad29
" TODO - get this plugin: https://github.com/w0rp/ale#usage-linting
augroup PythonLinting
    autocmd!
    autocmd FileType python setlocal makeprg=pylint3\ --output-format=parseable
    " uncomment this line to run make on save
    " autocmd BufWritePost *.py silent make! <afile> | silent redraw!
    autocmd QuickFixCmdPost [^l]* cwindow
augroup END

" {{{1 From ALE README
" Put these lines at the very end of your vimrc file.
" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL

" {{{1 CUSTOM FUZZY FINDER
"
" This is an in-file fuzzy finder which means that it also works for netrw
"
" This does multiple things:
"   1. DONE - add a .\{-} between each char to allow for any between them
"   2. TODO - accept two letters typed in the wrong order
"   3. TODO - accept things that cross newline boundaries?
"
" This function doesn't do anything if it was already use for the last search
fun! MyFuzzyFinder()
    let lastsearch = @/
    let newsearch = '\(fuzzfind\)\{0}'
    " TODO: figure out what '\zs' is
    for char in split(lastsearch, '\zs')
        " skip if this just ran in last search
        if match(lastsearch, 'fuzzfind') != -1
            let newsearch = lastsearch
            break
        endif
        let newsearch = newsearch . char . '.\{-}'
    endfor
    exe 'normal ?' . newsearch . "\<cr>" | set hlsearch
endfun
nnoremap <leader><leader> :call MyFuzzyFinder()<cr>/<up><cr>

" quickly add .\{-} to search
" not using right now, but this matches newlines too: \_.\{-}
" TODO: fix this because it is really bad when using command line
" cnoremap , .\{-}

" ASCII ART {{{1
fun! LineToAsciiArt()
    " save register and cursor position
    let save_pos = getpos(".")
    let olda = @a

    " delete without line break into reg a
    normal! 0"aD

    " add ascii art version
    let text = substitute(@a, '!', '\\!', '')
    exe "r! figlet '" . text . "'"

    " delete whitespace after ascii art
    " normal! 0"_D

    " restore register and cursor position
    call setpos(".", save_pos)
    let @a = olda

    " delete newline before ascii art and go to position below art
    normal! "_dd}
endfun
nnoremap <Leader>a :call LineToAsciiArt()<cr>

" open the current WORD with the bash 'open' command
"
" EXAMPLES
" --------
" http://duckduckgo.com
" ~/Documents/Schedule_Fall2017.pdf
nnoremap <leader>open "uyiW:!open <c-r>u<cr><cr>

" Visual motion from 'more instantly better vim'
runtime plugin/dragvisuals.vim

vmap  <expr>  <LEFT>   DVB_Drag('left')|noh
vmap  <expr>  <RIGHT>  DVB_Drag('right')|noh
vmap  <expr>  <DOWN>   DVB_Drag('down')|noh
vmap  <expr>  <UP>     DVB_Drag('up')|noh
vmap  <expr>  D        DVB_Duplicate()|noh

" Remove any introduced trailing whitespace after moving...
let g:DVB_TrimWS = 1

" {{{ ULTISNIPS

" private snippets directory
let g:UltiSnipsSnippetsDir = "~/.vim/UltiSnips"

" use google style docstrings
let g:ultisnips_python_style = "google"
