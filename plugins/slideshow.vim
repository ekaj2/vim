" this expects a file with a line starting with, e.g.:
" ```
" " 2>>>
" ```
" Then those lines get expanded into :
" ```
"
"
" ```
" 
" Then the function undos what it just did
"
" But done in an animation
fun! AddWhitespaceAnimation()
    let olda = @a
    let foundone = 0
    while search('[1-9]>>>')
        let foundone = 1

        " copy number into 'a' reg
        normal! ^w"ayl0D

        let numlines = @a - 1
        " add appropriate number of lines
        while numlines > 0
            " add a line
            normal! o

            " display change
            redraw
            sleep 250m
            
            " decrement counter
            let numlines -= 1
        endwhile
    endwhile

    " if we should undo, do it
    if foundone == 1
        normal! u
    endif

    " go to next buffer
    bn

    " if it has the markers, run them now
    if search('[1-9]>>>')
        " go to beginning of the file
        normal gg
        call AddWhitespaceAnimation()
    endif
endfun

" 1. Play animation
" 2. Undo changes
" 3. Go to next buffer
" 4. Put cursor at EOF
nnoremap <space> :call AddWhitespaceAnimation()<cr>GG
