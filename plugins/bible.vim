" Author: Jake Dube
" Date: 06/27/2018

fun! BibleReferenceSearch()
    call inputsave()
    let book = input('book: ')
    let chapter = input('chapter: ')
    let verse = input('verse: ')
    call inputrestore()

    " after chapter, find newline, then
    " anything, then a verse
    let search = '/' . book . '.*Chapter\s\{-}\<' . chapter . '\>\_s\_.\{-}' . verse
    execute search
    
    " go to end of last search
    normal! gn
endfun
nnoremap <leader>bib :call BibleReferenceSearch()<cr>

