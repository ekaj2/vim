" 2 space racket indentation
" start by storing the old values so that they can be restored
let old_tabstop=&tabstop
let old_shiftwidth=&shiftwidth
let old_autoindent=&autoindent
let old_expandtab=&expandtab
setlocal tabstop=2
setlocal shiftwidth=2
setlocal autoindent
setlocal expandtab

let b:undo_ftplugin = "set tabstop=" . old_tabstop . " " .
                    \ "| set shiftwidth=" . old_shiftwidth . " " .
                    \ "| set autoindent=" . old_autoindent . " " .
                    \ "| set expandtab=" . old_expandtab
