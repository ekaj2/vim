fun! tex#build()
    " run the command
    " pandoc  -o currentfile.html
    let outputfile = expand("%:t:r") . '.pdf'
    let fullcmd = 'pandoc ' . expand("%:p") . ' --output=' . outputfile

    call GoToFolderAndRunThenReturn(fullcmd)
endfun
nnoremap <buffer> <LocalLeader>bld :w<cr>:call tex#build()<cr><cr>

fun! tex#view()
    let builtfile = expand("%:t:r") . '.pdf'
    let fullcmd = 'open ' . builtfile

    call GoToFolderAndRunThenReturn(fullcmd)
endfun

" open the built file in the appropriate program
nnoremap <buffer> <LocalLeader>view :w<cr>:call tex#view()<cr><cr>

" open wolfram alpha
nnoremap <buffer> <LocalLeader>wa :!open https://www.wolframalpha.com<cr><cr>

" add external command mappings to these internal functions
command! -bar Build call tex#build()
command! -bar View call tex#view()

" autowrap lines at 80-characters
setlocal tw=80
