" Filetype plugin for QuickNOTE files
" this file type is meant to allow faster typing for, e.g., class notes

" BEGINNOTE
"   * TODO - Look up those definitions in Wiktionary
"   * TODO - Vet dictionary list with Wiktionary
" ENDNOTE

let s:dicts_folder = expand("<sfile>:h")
if has('win32')
    let s:dicts_folder = s:dicts_folder."\\..\\dicts\\"
else
    let s:dicts_folder = s:dicts_folder."/../dicts/"
endif

set spell
set wrap
set breakindent

fun! EnableAutoComplete(completiontype)
    for mystr in split("a b c d e f g h i j k l m n o p q r s t u v w x y z")
        exe "inoremap <buffer> ".mystr." ".mystr.a:completiontype
    endfor
endfun
command! EnableSpellComplete call EnableAutoComplete("<c-x>s<c-p>")
command! EnableDictComplete call EnableAutoComplete("<c-x><c-k><c-p>")
command! EnableEngComplete call EnableAutoComplete("<c-x><c-u><c-n><c-p>")

fun! DisableAutoComplete()
    for mystr in split("a b c d e f g h i j k l m n o p q r s t u v w x y z")
        exe "iunmap <buffer> ".mystr
    endfor
endfun
command! DisableAutoComplete call DisableAutoComplete()

fun! CompleteEnglish(findstart, base)
    if a:findstart
        " locate the start of the word
        let line = getline('.')
        let start = col('.') - 1
        while start > 0 && line[start - 1] =~ '\a'
            let start -= 1
        endwhile

        " skip until we have 3 characters or more
        if col('.') - start <= 3
            return -2
        endif
        return start
    else
        " store matches in this list
        let res = []

        " get path to appropriate dictionary file, and check if it exists
        let dictFile = s:dicts_folder.a:base[0:2].'.html'
        if !filereadable(dictFile)
            " no matches
            echom dictFile." does not exist"
            return []
        endif

        " open, e.g., k.html and parse it
        let mydict = readfile(dictFile)

        " TODO - prioritize longer words, cs words, and demote ones from
        " oversized_dictionary
        " TODO - use slow handler thing see :h complete-functions
        " TODO - add new feature to each file that sets priority/rank
        "      - sort list accordingly
        for m in mydict
            " TODO: strip m string of trailing whitespace
            if match(m, ".%.") == -1
                echom m
                continue
            endif
            let [word, definition] = split(m, "%")
            if m =~ '^' . a:base
                call add(res, {'word': word, 'info': definition})
            endif
        endfor
        return res
    endif
endfun
set completefunc=CompleteEnglish

