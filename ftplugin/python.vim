fun! PythonFromImportSnippet()
    call inputsave()
    let v_from = input('from: ')
    let v_import = input('import: ')
    let v_as = input('as: ')
    call inputrestore()

    if strlen(v_from) == 0
        " skip if user doesn't define 'from'
        startinsert!
        return
    elseif strlen(v_import) == 0
        " from x import|
        exe "normal! afrom " . v_from . " import "
    elseif strlen(v_as) == 0
        " from x import y|
        exe "normal! afrom " . v_from . " import " . v_import
    else
        " from x import y as z|
        exe "normal! afrom " . v_from . " import " . v_import . " as " . v_as
    endif

    startinsert!
endfun

fun! PythonOpenFileSnippet()
    call inputsave()
    let file = input('file: ')
    let mode = input('mode: ')
    let var = input('variable: ')
    call inputrestore()

    if strlen(mode) == 0
        let mode = "w"
    endif
    if strlen(var) == 0
        let var = "f"
    endif

    " add with open line
    exe "normal! awith open(\"" . file . "\", '" . mode . "') as " . var .  ":\<esc>"

    " change content inside of with block based on if reading or writing
    if mode == "w" || mode == "a" || mode == "wb"
        " add print() statement
        exe "normal! oprint(, file=" . var . ")\<esc>^wl"
        startinsert
    else
        exe "normal! ocontents = " . var . ".read"
        startinsert!
    endif
endfun

" abbreviationish mappings
inoremap <buffer> sf self.
inoremap <buffer> pri print()<left>
inoremap <buffer> wio <esc>:call PythonOpenFileSnippet()<cr>
inoremap <buffer> frim <esc>:call PythonFromImportSnippet()<cr>

" abbreviations
iabbr <buffer> defmain def main():  # pylint: disable=missing-docstring<cr><cr><cr><cr><c-u>if __name__ == "__main__":<cr>main()<up><up><up><up>   
iabbr <buffer> imp import
iabbr <buffer> cls class
iabbr <buffer> _in def __init__():
iabbr <buffer> br break
iabbr <buffer> rtn return
iabbr <buffer> fr from
iabbr <buffer> def def():<left><left><left>

" TODO - figure out why I can't do iunmap here

let b:undo_ftplugin = "iunabbrev <buffer> defmain " .
                    \ "| iunabbrev <buffer> imp " .
                    \ "| iunabbrev <buffer> cls " .
                    \ "| iunabbrev <buffer> _in "
                    \ "| iunabbrev <buffer> br " .
                    \ "| iunabbrev <buffer> return " .
                    \ "| iunabbrev <buffer> fr " .
                    \ "| iunabbrev <buffer> def " .
