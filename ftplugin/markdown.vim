" Author: Jake Dube
" Simple Markdown mappings that will eventually grow into functions

" TODO - restore register h at end in all functions

" add qnote stuff
let s:qnote_vimscript = expand("<sfile>:h")
if has('win32')
    let s:qnote_vimscript = s:qnote_vimscript."\\qnote.vim"
else
    let s:qnote_vimscript = s:qnote_vimscript."/qnote.vim"
endif
execute "source" s:qnote_vimscript

let maplocalleader = '--'

set wrap

" 3 title levels
inoremap <buffer> <LocalLeader>t1 <esc>yypVr=o<cr>
inoremap <buffer> <LocalLeader>t2 <esc>yypVr-o<cr>
inoremap <buffer> <LocalLeader>t3 <esc>^3i#<esc>li <esc>o<cr>

" bulleted list (use * so that the dash is for definitions, e.g.,
" * item A - this item is particularly interesting
fun! MarkdownNewline()
    " mark current cursor position and then copy the first char of this line
    " into register h to check if it is an asterisk (bullet)
    " TODO - don't be an idiot and mark in the 'h' register that you are using
    exe "normal! \<esc>mh^\"hyl"
    if @h == "*"
        exe "normal! `ha\<cr>* \<esc>"
        startinsert!
    "elseif @h == "|"
    "    call NextRow()
    else
        exe "normal! `ha\<cr>\<esc>"
        startinsert!
    endif
endfun
fun! Remapped_o()
    " copy first character into register
    normal! ^"hyl
    if @h == "*"
        normal! o* 
        startinsert!
    "elseif @h == "|"
    "    call NextRow()
    else
        normal! o
        startinsert!
    endif
endfun
fun! Remapped_O()
    " copy first character into register
    normal! ^"hyl
    if @h == "*"
        normal! O* 
        startinsert!
    "elseif @h == "|"
    "    call NextRow()
    else
        normal! O
        startinsert!
    endif
endfun
inoremap <buffer> <cr> <esc>:call MarkdownNewline()<cr>
nnoremap <buffer> o :call Remapped_o()<cr>
nnoremap <buffer> O :call Remapped_O()<cr>
inoremap <buffer> <LocalLeader><cr> <cr>
nnoremap <buffer> <LocalLeader>o o
nnoremap <buffer> <LocalLeader>O O

" spelling stuff
setlocal spell
function! QuickFixLastSpellingError()
    " save current position
    let save_pos = getpos(".")
    let maxcolumn = strlen(getline("."))

    " actual operation
    normal! [s1z=

    " restore cursor position
    call setpos(".", save_pos)
    
    " enter insert mode at the right place
    if save_pos[2] == maxcolumn
        startinsert!
    else
        startinsert
    endif
endfunction

" quick fix spelling err
inoremap <buffer> <LocalLeader>sp <esc>:call QuickFixLastSpellingError()<cr>

" full spelling fix operation
nnoremap <buffer> <LocalLeader>sp <esc>[sz=

" TODO - fix multi-line selection issues
" bold, italics, code
fun! ItalicizeSelection()
    exe "normal! `<i*\<esc>`>l"
    normal! a*
endfun
fun! CodifySelection()
    exe "normal! `<i`\<esc>`>l"
    normal! a`
endfun
vnoremap <buffer> <LocalLeader>i :call ItalicizeSelection()<cr>
vnoremap <buffer> <LocalLeader>em <esc>`<i**<esc>`>2la**<esc>
vnoremap <buffer> <LocalLeader>c :call CodifySelection()<cr>
inoremap <buffer> <LocalLeader>i <esc>mhviw<esc>`<i*<esc>`>la*<esc>`h2la
inoremap <buffer> <LocalLeader>em <esc>mhviw<esc>`<i**<esc>`>2la**<esc>`h4la
inoremap <buffer> <LocalLeader>c <esc>mhviw<esc>`<i`<esc>`>la`<esc>`h2la

" bulleting
" TODO - use s: prefix for local functions
" see :h eval.txt
fun! BulletAddStar()
    if !CanHaveStar(line('.'))
        return 0
    endif

    " add '* ' to beginning of line (ignoring whitespace)
    execute "normal ^i* \<esc>"
    return 1
endfun

fun! BulletStarRemove()
    " remove entirely
    " the F- is added to stop execution if this isn't a bullet
    execute "normal ^lF*xx\<esc>"
endfun

fun! CanHaveStar(line)
    " move cursor to current line
    call cursor(a:line, 1)

    " skip if it is a blank line
    if strlen(getline(a:line)) == 0
        return 0
    endif

    " copy first character into register
    normal! ^"hyl

    " skip if it is a header with #
    " skip if it is already a bullet type
    " skip if it is a table row
    if @h == "*" || @h == "-" || @h == "#" || @h == "|"
        return 0
    endif

    " copy first 3 non-whitespace characters of next line 
    " into register and return cursor to previous line
    normal! j^"h3ylk

    " skip if it is a header with at least 3:
    " --- or ===
    if @h == "---" || @h == "==="
        return 0
    endif

    return 1
endfun

fun! BulletizeSelectionComplex() range
    let canHaveStar = 0
    for line in range(line("'<"),line("'>"))
        let canHaveStar += CanHaveStar(line)
    endfor

    if canHaveStar
        '<,'>call BulletAddStar()
    else
        '<,'>call BulletStarRemove()
    endif
endfun

fun! BulletizeLineComplex()
    let save_pos = getpos(".")
    let origcolumn = save_pos[2]
    let maxcolumn = strlen(getline("."))

    " copy first non-whitespace character into register
    normal! ^"hyl

    if @h == "*"
        call BulletStarRemove()

        " restore cursor position
        let save_pos[2] -= 1
    else
        let success = BulletAddStar()
        if success
            " restore cursor position
            let save_pos[2] += 3
        else
            echom "Cannot add star to this line..."
        endif
    endif
    
    " restore cursor position
    if origcolumn == 1
        let save_pos[2] -= 1
    endif
    call setpos(".", save_pos)
    if origcolumn == maxcolumn
        startinsert!
    else
        startinsert
    endif
endfun

vnoremap <buffer> <LocalLeader>b :call BulletizeSelectionComplex()<cr><cr>
inoremap <buffer> <LocalLeader>b <esc>:call BulletizeLineComplex()<cr>

" TODO - separate number sequence based on indent levels
" TODO - add ability to number indented text
" number selection
fun! NumberSelection() range
    let i = 1
    for line in range(line("'<"),line("'>"))
        " move cursor to current line at first column
        call cursor(line, 1)
        exe "normal! ^i" . i . ". "

        " increment counter
        let i += 1
    endfor
endfun

vnoremap <buffer> <LocalLeader>n :call NumberSelection()<cr>

" basic implementation of tables
" TODO - go up and down (row to row)
" TODO - don't add a new line if right above the --- (that should only
"        be a header)
" TODO - add column at beginning, middle, and end
" TODO - move column/row left,right/up,down
" TODO - scale table with text insertion
" TODO - add header (| --- | --- |) defining row if '| ' is at the beginning
"        of this line, but not the one above it
fun! NextRow()
    " copy first char from previous line into register h, then return cursor
    " to the current line
    normal! k^"hylj
    
    if @h == "|"
        call AddBlankRow()
    else
        call AddHeaderRow()
        call AddBlankRow()
    endif

    normal! ^2l
    startreplace
endfun

fun! AddBlankRow()
    " meant to be run before moving on to next line
    " copy and clear everything but '|'
    normal! yyp
    let i = 0
    while i < strlen(getline(line(".")))
        " copy next char into register h
        normal! "hyl

        " make it a space if not a '|'
        if @h != "|"
            exe "normal! r "
        endif

        " go to next character
        normal! l
        let i += 1
    endwhile
endfun

fun! AddHeaderRow()
    " ensure there is a blank line below table
    " TODO - check first, don't just add
    exe "normal! o\<esc>k"
    normal! yyp
    let i = 0
    while i < strlen(getline(line(".")))
        " copy next char into register h
        normal! "hyl

        if @h == "|"
            " make previous and next characters spaces
            " because we did an extra character, need to increment 'i'
            "
            " edge cases:
            " 1. first '|' may not be able to do char replacement to its left
            " 2. issue with last line?

            " first column
            if getcurpos()[2] == 1
                exe "normal! lr "
            else
                exe "normal! hr 2lr "
            endif
            let i += 1
        else
            " otherwise, make it a '-' if not a '|'
            exe "normal! r-"
        endif

        " go to next character
        normal! l
        let i += 1
    endwhile
endfun

" move right one table slot
inoremap <buffer> <LocalLeader>l <esc>f<bar>2lR
" move left one table slot
inoremap <buffer> <LocalLeader>h <esc>F<bar>F<bar>2lR

fun! RestoreCursor(origcolumn, save_pos, maxcolumn)
    if a:origcolumn == 1
        let a:save_pos[2] -= 1
    endif
    call setpos(".", a:save_pos)
    if a:origcolumn == a:maxcolumn
        startinsert!
    else
        startinsert
    endif
endfun

" auto resize column when text goes too far
" TODO - fix issue with two-letter mapping
" TODO - improve table bounds (search for lines starting with |, don't use {)
fun! ResizeColumnIfNeeded()
    let oldh = @h
    let oldg = @g
    let oldf = @f
    let maxcolumn = strlen(getline("."))
    let save_pos = getpos(".")
    let origcolumn = save_pos[2]

    " see if we are in a table
    " check if the first char of this line and the previous are '|'
    " check if the cursor is on the 5th or higher column (TODO - refine)
    exe "normal! \<esc>^\"hyl"
    exe "normal! \<esc>k^\"gylj"
    call setpos(".", save_pos)
    if @h != "|" || @g != "|" || origcolumn < 5
        call RestoreCursor(origcolumn,save_pos,maxcolumn)
        return
    endif

    " go one and two characters to the right and see if either is a '|'
    " then return the cursor
    exe "normal! \<esc>2l\"hyl2h"
    exe "normal! \<esc>l\"gylh"
    exe "normal! \<esc>\"fyl"
    if @h != '|' && @g != '|' && @f != '|'
        call RestoreCursor(origcolumn,save_pos,maxcolumn)
        return
    endif
    echom "Need to resize column..."
    " save cursor position
    let save_pos = getpos(".")

    " get line of beginning and end of table (do ending first so that we
    " can start at the beginning)
    normal! }k
    let lineb = line(".")
    normal! {j
    let linea = line(".")

    " select the offending '|' wall
    let currline = line(".")

    " find the column...
    " could be save_pos[2] +1 or +2
    let barcol = save_pos[2]
    exe "normal! " . barcol . "\<bar>f\<bar>"
    let barcol = getpos(".")[2]
    if @f == '|'
        let barcol = save_pos[2]
    endif
    while currline <= lineb
        " insert 3 spaces before '|' at appropriate column
        exe "normal! " . barcol . "\<bar>"
        exe "normal! 3i \<esc>"

        " move to next line
        normal! j
        let currline = line(".")
    endwhile

    " restore cursor position
    call setpos(".", save_pos)
    let @h = oldh
    let @g = oldg
    let @f = oldf
endfun
" au TextChangedI *.md call ResizeColumnIfNeeded()

" TODO - figure references

" TODO - expose the path to github.css as a 'global' variable at the top of
"        this document
" TODO - check if github.css exists see also^^^, else echo error message
fun! markdown#build()
    " run the command
    " pandoc -f markdown currentfile.md -o currentfile.html
    let outputfile = expand("%:t:r") . '.html'
    let fullcmd = 'mkdir -p renders && pandoc ' . expand("%:p") . ' --output=renders/' . outputfile . ' --css=$HOME/.vim/resources/markdown-css-master/github.css --self-contained --highlight-style=tango'

    " condensed version of function found in .vim/vimrc:
    " 
    " fun! GoToFolderAndRunThenReturn(mycommand)
    "     let gitdir = expand("%:h")
    "     exe 'cd' fnameescape(gitdir)
    "     exe ":!" . a:mycommand
    "     silent! cd -
    " endfun

    call GoToFolderAndRunThenReturn(fullcmd)

    " execute fullcmd
endfun
nnoremap <buffer> <LocalLeader>bld :w<cr>:call markdown#build()<cr><cr>

fun! markdown#view()
    let builtfile = 'renders/' . expand("%:t:r") . '.html'
    let fullcmd = 'open ' . builtfile

    call GoToFolderAndRunThenReturn(fullcmd)
endfun
" open the built file in the appropriate program
nnoremap <buffer> <LocalLeader>view :w<cr>:call markdown#view()<cr><cr>

" add abbreviations
" >>>ABBREVIATE START<<<
iabbr <buffer> teh the
iabbr <buffer> hte the
iabbr <buffer> i I
iabbr <buffer> io I
iabbr <buffer> im I'm
iabbr <buffer> Im I'm
iabbr <buffer> Im' I'm
iabbr <buffer> im' I'm
iabbr <buffer> ive I've
iabbr <buffer> gt going to
iabbr <buffer> goin going
iabbr <buffer> nad and
iabbr <buffer> adn and
iabbr <buffer> becuase because
iabbr <buffer> becuas because
iabbr <buffer> becaus because
iabbr <buffer> bc because
iabbr <buffer> freind friend
iabbr <buffer> freinds friends
iabbr <buffer> si is
iabbr <buffer> tihs this
iabbr <buffer> fisrt first
iabbr <buffer> lolipop lollipop
iabbr <buffer> var variable
iabbr <buffer> vars variables
iabbr <buffer> bw between
iabbr <buffer> bc because
iabbr <buffer> mu μ
iabbr <buffer> sigma σ
iabbr <buffer> alpha α
iabbr <buffer> theta Θ
iabbr <buffer> delta Δ
iabbr <buffer> beta β
iabbr <buffer> pi π
iabbr <buffer> Sigma Σ
" >>>ABBREVIATE END<<<

" TODO - see about autocmd in undo_ftplugin
" cleanup stuff
let b:undo_ftplugin = "iunmap <buffer> <LocalLeader>t1 " .
                    \ "| iunmap <buffer> <LocalLeader>t2 " .
                    \ "| iunmap <buffer> <LocalLeader>t3 "
                    \ "| iunmap <buffer> <cr> " .
                    \ "| unmap <buffer> o " .
                    \ "| unmap <buffer> O " .
                    \ "| iunmap <buffer> <LocalLeader><cr> " .
                    \ "| unmap <buffer> <LocalLeader>o " .
                    \ "| unmap <buffer> <LocalLeader>O " .
                    \ "| iunmap <buffer> <LocalLeader>sp " .
                    \ "| vunmap <buffer> <LocalLeader>i " .
                    \ "| vunmap <buffer> <LocalLeader>em " .
                    \ "| vunmap <buffer> <LocalLeader>c " .
                    \ "| iunmap <buffer> <LocalLeader>i " .
                    \ "| iunmap <buffer> <LocalLeader>em " .
                    \ "| iunmap <buffer> <LocalLeader>c " .
                    \ "| unmap <buffer> <LocalLeader>pan " .
                    \ "| vunmap <buffer> <LocalLeader>b " .
                    \ "| iunmap <buffer> <LocalLeader>b " .
                    \ "| vunmap <buffer> <LocalLeader>n " .
                    \ "| iunmap <buffer> <LocalLeader>l " .
                    \ "| iunmap <buffer> <LocalLeader>h " .
                    \ "| set nospell " .
                    \ "| set nowrap " .
" >>>UNABBREVIATE START<<<
                    \ " iunabbr <buffer> teh " .
                    \ " iunabbr <buffer> hte " .
                    \ " iunabbr <buffer> i " .
                    \ " iunabbr <buffer> io " .
                    \ " iunabbr <buffer> im " .
                    \ " iunabbr <buffer> Im " .
                    \ " iunabbr <buffer> Im' " .
                    \ " iunabbr <buffer> im' " .
                    \ " iunabbr <buffer> ive " .
                    \ " iunabbr <buffer> gt " .
                    \ " iunabbr <buffer> goin " .
                    \ " iunabbr <buffer> nad " .
                    \ " iunabbr <buffer> adn " .
                    \ " iunabbr <buffer> becuase " .
                    \ " iunabbr <buffer> becuas " .
                    \ " iunabbr <buffer> becaus " .
                    \ " iunabbr <buffer> bc " .
                    \ " iunabbr <buffer> freind " .
                    \ " iunabbr <buffer> freinds " .
                    \ " iunabbr <buffer> si " .
                    \ " iunabbr <buffer> tihs " .
                    \ " iunabbr <buffer> fisrt " .
                    \ " iunabbr <buffer> lolipop " .
                    \ " iunabbr <buffer> var " .
                    \ " iunabbr <buffer> vars " .
                    \ " iunabbr <buffer> bw " .
                    \ " iunabbr <buffer> bc " .
                    \ " iunabbr <buffer> mu " .
                    \ " iunabbr <buffer> sigma " .
                    \ " iunabbr <buffer> alpha " .
                    \ " iunabbr <buffer> theta " .
                    \ " iunabbr <buffer> delta " .
                    \ " iunabbr <buffer> beta " .
                    \ " iunabbr <buffer> pi " .
                    \ " iunabbr <buffer> Sigma " .
" >>>UNABBREVIATE END<<<
                    \ ""
